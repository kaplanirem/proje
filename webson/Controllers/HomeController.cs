﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace webson.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }


        public ActionResult Iletisim()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        public ActionResult Kategori()
        {
            return View();
        }
        public ActionResult Kitap()
        {
            return View();
        }
        public ActionResult Yazar()
        {
            return View();

        }
    }
}