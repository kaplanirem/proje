﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(webson.Startup))]
namespace webson
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
